<div class="container">
    <h2>Image</h2>
    <p>The .img-responsive class makes the image scale nicely to the parent element (resize the browser window to see the effect):</p>
    <a href="{$link->getModuleLink('testmodule', 'showproducts', [], true)|escape:'html'}" title=""><img src="http://www.w3schools.com/bootstrap/cinqueterre.jpg" class="img-responsive" alt="Cinque Terre" width="304" height="236"> </a>
</div>