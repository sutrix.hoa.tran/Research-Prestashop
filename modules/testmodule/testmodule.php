<?php
/**
 * Created by PhpStorm.
 * User: hoa.tran
 * Date: 25/08/2016
 * Time: 16:49
 */
class TestModule extends Module
{

    public function __construct()
    {
        $this->uninstall();
        $this->name = 'testmodule';
        $this->tab = 'front_office_features';
        $this->version = '0.1';
        $this->author = 'Hoa Tran';
        $this->displayName = "testmodule";
        $this->description = 'With this module';
        parent::__construct();
    }

    public function install()
    {
        parent::install();
        $this->registerHook('displayFooter');
        $this->registerHook('displayRightColumn');
        return true;
    }

    public function hookDisplayFooter($params)
    {
//        $this->context->smarty->assign('urlImage',$this->module->getPathUri());
        return $this->display(__FILE__, 'testContent.tpl');
    }
    public function hookDisplayRightColumn($params)
    {
//        $this->context->smarty->assign('urlImage',$this->module->getPathUri());
        return $this->display(__FILE__, 'testContent.tpl');
    }
}