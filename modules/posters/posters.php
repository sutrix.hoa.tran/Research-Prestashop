<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
if (!defined('_PS_VERSION_'))
	exit;
require_once(dirname(__FILE__) . '/classes/PostersModel.php');
class Posters extends Module
{
    public $rightColumn = 'right_column';
    public $leftColumn = 'left_column';
    public $header = 'header';
    public $top = 'top';
    public $footer = 'footer';         
    public $home = 'home';             
    public function __construct()
    {
        $this->name = 'posters';
        $this->tab = 'front_office_features';
        $this->version = '0.1';
        $this->author = 'Xuan Hoa';
        $this->displayName = $this->l('Sutrix Posters');
        $this->description = $this->l('This is module allow you add posters to your shop');
        parent::__construct();
    }
    
    public function install()
    {
        if(!parent::install() || !$this->createTables() ||
          !$this->registerHook('displayLeftColumn') || 
             !$this->registerHook('displayRightColumn') ||
             !$this->registerHook('displayFooter') ||
             !$this->registerHook('displayHeader') ||
             !$this->registerHook('displayHome') ||
             !$this->registerHook('displayTop')  
                ){
        
            return false;
        }

        if(!$this->installTab('Home', 'AdminPosters', 'Posters')){
            return false;
        }
        if(Shop::isFeatureActive()){
            Shop::setContext(Shop::CONTEXT_ALL);
        }
        
        $this->img_folder = _PS_IMG_DIR_.'pos';
        
        if(!is_dir($this->img_folder) && !file_exists($this->img_folder))
	{
            mkdir($this->img_folder);
            chown($this->img_folder,fileowner(_PS_IMG_DIR_));
            chgrp($this->img_folder,filegroup(_PS_IMG_DIR_));
            chmod($this->img_folder,fileperms(_PS_IMG_DIR_));
	}
        return true;
            
    }
    
    public function getPostersByHook($hook)
    {
                 
        if(!empty($hook)){
             $posters =array();
             $idLang =  $this->context->language->id;
             $idShop = $this->context->shop->id;
             $posters = PostersModel::getAllPostersByHook($hook, $idLang, $idShop);
        
             if($posters)
     
             foreach ($posters as $i => $poster){
        
                if(!empty($poster['show_page'])){
                    $controllerCurrent = Dispatcher::getInstance()->getController();
                        $showPage = @unserialize($poster['show_page']);
                    if(!in_array($controllerCurrent, (array)$showPage)){
                        unset($posters[$i]);
                    }
      
                } 
            
//                $pageShowPosters =  PostersModel::getPagesOfPosters($poster->id);
             }

             
        
             return $posters;
        }
        
        return false;
   
    }
    
    public function hookDisplayRightColumn($param)
    {

        $posters = $this->getPostersByHook($this->rightColumn);
        $controller =  $this->getHookController('showPosters');
        return (!empty($posters))? $controller->run($param, $posters) : null;
    }
    
    public function hookDisplayFooter($param)
    {
//                $controller = Dispatcher::getInstance()->getController();
////        $idCms = Tools::getValue('id_cms');
//        var_dump($controller);
//        exit;
        $posters = $this->getPostersByHook($this->footer);
        $controller =  $this->getHookController('showPosters');
        return (!empty($posters))? $controller->run($param, $posters) : null;
    }
    public function hookDisplayHeader($param)
    {
        $posters = $this->getPostersByHook($this->header);
        $controller =  $this->getHookController('showPosters');
        return (!empty($posters))? $controller->run($param, $posters) : null;
    }
    public function hookDisplayTop($param)
    {
         $posters = $this->getPostersByHook($this->top);
        $controller =  $this->getHookController('showPosters');
        return (!empty($posters))? $controller->run($param, $posters) : null;
    }
    public function hookDisplayHome($param)
    {
         $posters = $this->getPostersByHook($this->home);
        $controller =  $this->getHookController('showPosters');
        return (!empty($posters))? $controller->run($param, $posters) : null;
    }
    public function hookDisplayLeftColumn($param)
    {
        $posters = $this->getPostersByHook($this->leftColumn);
        $controller =  $this->getHookController('showPosters');
        return (!empty($posters))? $controller->run($param, $posters) : null;
    }
    
    public function getHookController($hook_name)
    {
// Include the controller file
        require_once(dirname(__FILE__) . '/controllers/hook/' . $hook_name . '.php');
// Build the controller name dynamically
        $controller_name = $this->name . $hook_name . 'Controller';
// Instantiate controller
        $controller = new $controller_name($this, __FILE__, $this->_path);
// Return the controller
        return $controller;
    }
    public function dropTables()
    {
        $res = Db::getInstance()->execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'posters`,`'._DB_PREFIX_.'posters_lang`,`'._DB_PREFIX_.'posters_shop`,`'._DB_PREFIX_.'type_posters`;');
    
        return $res;
    }
    
    
    public function createTables()
    {
        //Create table posters
        $result1 = Db::getInstance()->execute('CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'posters`  (
	`id_posters` INT(11) NOT NULL AUTO_INCREMENT,
	`id_type_posters` INT(11) NOT NULL,
	`image` VARCHAR(100) NULL DEFAULT NULL,
	`target_link` VARCHAR(100) NULL DEFAULT NULL,
	`position_hook` VARCHAR(50) NOT NULL,
	`source_link` VARCHAR(100) NULL DEFAULT NULL,
	`width` INT(7) NULL DEFAULT NULL,
	`height` INT(7) NULL DEFAULT NULL,
	`active` TINYINT(1) NOT NULL DEFAULT \'1\',
	`show_page` VARCHAR(100) NULL DEFAULT NULL,
	`id_product` VARCHAR(100) NULL DEFAULT NULL,
	`id_category` INT(11) NULL DEFAULT NULL,
	`product_limit` INT(11) NULL DEFAULT NULL,
	`position` INT(11) NULL DEFAULT NULL,
	PRIMARY KEY (`id_posters`)) COLLATE=\'utf8_general_ci\' ENGINE=InnoDB;');
        
        //create posters_lang table
        $result2 =  Db::getInstance()->execute('CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'posters_lang` (
	`id_posters` INT(11) NOT NULL,
	`id_lang` INT(11) NOT NULL,
	`name` VARCHAR(100) NOT NULL,
	`caption` VARCHAR(100) NULL DEFAULT NULL,
	`description` VARCHAR(100) NULL DEFAULT NULL,
	PRIMARY KEY (`id_posters`, `id_lang`) )COLLATE=\'utf8_general_ci\'ENGINE = InnoDB;');
        
        //create posters_shop table
        $result3 = Db::getInstance()->execute('CREATE TABLE IF NOT EXISTS  `'._DB_PREFIX_.'posters_shop` (
	`id_posters` INT(11) NOT NULL,
	`id_shop` INT(11) NOT NULL,
	PRIMARY KEY (`id_posters`, `id_shop`) ) COLLATE=\'utf8_general_ci\' ENGINE=InnoDB;');
        
        $result4 = Db::getInstance()->execute('CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'type_posters` (
	`id_type_posters` INT(7) NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(100) NULL DEFAULT NULL,
	PRIMARY KEY (`id_type_posters`)
        )COLLATE= \'utf8_general_ci\'
        ENGINE=InnoDB
        AUTO_INCREMENT=1;');
        
        $result5 = Db::getInstance()->execute('INSERT INTO `prestashop`.`ps_type_posters` (`name`) VALUES (\'image\');
            INSERT INTO `prestashop`.`ps_type_posters` (`name`) VALUES (\'video\');
            INSERT INTO `prestashop`.`ps_type_posters` (`name`) VALUES (\'product\');
            INSERT INTO `prestashop`.`ps_type_posters` (`name`) VALUES (\'category\');
            INSERT INTO `prestashop`.`ps_type_posters` (`name`) VALUES (\'best_sale\');
            INSERT INTO `prestashop`.`ps_type_posters` (`name`) VALUES (\'most_view\');');
        return $result1 && $result2 && $result3 && $result4;     
    }
    public function uninstall()
    {
        if (!parent::uninstall() ||
            !Configuration::deleteByName('posters')
        )
            return false;
        if (!$this->uninstallTab('AdminPosters') || !$this->dropTables())
            return false;
        
        
        return true;
    
        
    
    }
    
    public function uninstallTab($class_name)
    {
        $id_tab = (int)Tab::getIdFromClassName($class_name);
        $tab = new Tab((int)$id_tab);

        return $tab->delete();
    }
    
    public function installTab($parentTab, $class, $name)
    {
        $tab = new Tab();
        $tab->id_parent =  (int)Tab::getIdFromClassName($parentTab);
        $tab->name =  array();
        foreach (Language::getLanguages(true) as $lang ){
            $tab->name[$lang['id_lang']] = $name;  
        }
        $tab->class_name =  $class;
        $tab->module = $this->name;
        $tab->active =  1;
        return $tab->add();
    }
    
     public function templateAutocomplete($id_product = 0)
    {
		$this->context->controller->addCSS('/js/jquery/plugins/autocomplete/jquery.autocomplete.css', 'all');
		$this->context->controller->addJS($this->_path.'views/js/posters.js');
		$this->context->controller->addJS('/js/jquery/plugins/autocomplete/jquery.autocomplete.js');
        $my_associations = Posters::getAssociationsLight($this->context->language->id, $id_product); //function that will retrieve the array of all the product associated on my module table.
         $this->context->smarty->assign(array(
                        'my_associations' => $my_associations
                    ));

          return $this->context->smarty->fetch(dirname(__FILE__).'/views/templates/admin/autocomplete.tpl');
    }

	public static function getAssociationsLight($id_lang, $idPosters)
	{
  
            $res =array();
            if(!empty($idPosters) && $idPosters > 0){
            $id_products =  Db::getInstance()->getValue('SELECT p.id_product FROM '._DB_PREFIX_.'posters p WHERE p.id_posters = '.(int)$idPosters);

//            $id_products = explode(',', $id_products);   
            $id_products = rtrim($id_products, ",") ;
               if(!empty($id_products) && $id_products != ""){
                    $sql = 'SELECT p.`id_product`, p.`reference`, pl.`name`
                    FROM `'._DB_PREFIX_.'product` p 
                    '.Shop::addSqlAssociation('product', 'p').'
                    LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (
                    p.`id_product` = pl.`id_product`
                    AND pl.`id_lang` = '.(int)$id_lang.Shop::addSqlRestrictionOnLang('pl').'
                    ) WHERE p.`id_product` IN  ('.$id_products.')';
                    
                    $res = Db::getInstance()->executeS($sql);
               }
		return $res;
            }      
	}
     
}