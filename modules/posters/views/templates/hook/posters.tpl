
{if isset($posters)}
{*    <div class="container">*}
    {foreach from = $posters item = poster }
        
            
         {if $poster.id_type_posters == 1}
          <h3>{$poster.posters_name}</h3>
          <p>This is Posters of Sutrix Shop</p>
          
          <a href="{$poster.target_link|escape: 'html':'UTF-8'}" target="_blank">
          <img class="img-responsive" src="{$imageUrl}{$poster['id_posters']}.jpg" alt="Chania" >
          </a>
{*            {include file="themes/default-bootstrap/product-list.tpl" products=$products}*}

    {elseif $poster.id_type_posters == 2}
    <h3>{$poster.posters_name}</h3>
        <iframe  src="//www.youtube.com/embed/_5cCKgbax9E" frameborder="0" allowfullscreen></iframe>
    {elseif $poster.id_type_posters == 3}
     <h3>{$poster.posters_name}</h3>
        {if isset($products)}
            {foreach from =$products item = product}
         
				<a href="{$product.link|escape:'html':'UTF-8'}" title="" class="products-block-image content_img clearfix">
					<img class="replace-2x img-responsive" 
                                             src="{$link->getImageLink($product.link_rewrite, $product.id_image, 'home_default')|escape:'html':'UTF-8'}"
{*                                             src="{$link->getImageLink($special.link_rewrite, $special.id_image, 'small_default')|escape:'html':'UTF-8'}" *}
                                             alt=""> 
				</a>
				<div class="product-content">
                	<h5>
                    	<a class="product-name" href="{$product.link|escape:'html':'UTF-8'}" title="">
                            {$product.name|truncate:45:'...'|escape:'html':'UTF-8'}
                        </a>
                    </h5>
                    <p class="product-description">{$product.description_short|strip_tags:'UTF-8'|truncate:360:'...'}</p>
                                            <div class="price-box">
                            <span class="price">{$product.price}</span>
                            
                        </div>
                                    </div>
			
              {/foreach}    
       {/if}
    {elseif $poster.id_type_posters == 4}
            <h3>{$poster.posters_name}</h3>
        {if isset($productsCate)}
            {foreach from =$productsCate item = product}
         
				<a href="{$product.link|escape:'html':'UTF-8'}" title="" class="products-block-image content_img clearfix">
					<img class="replace-2x img-responsive" 
                                             src="{$link->getImageLink($product.link_rewrite, $product.id_image, 'home_default')|escape:'html':'UTF-8'}"
{*                                             src="{$link->getImageLink($special.link_rewrite, $special.id_image, 'small_default')|escape:'html':'UTF-8'}" *}
                                             alt=""> 
				</a>
				<div class="product-content">
                	<h5>
                    	<a class="product-name" href="{$product.link|escape:'html':'UTF-8'}" title="">
                            {$product.name|truncate:45:'...'|escape:'html':'UTF-8'}
                        </a>
                    </h5>
                    <p class="product-description">{$product.description_short|strip_tags:'UTF-8'|truncate:360:'...'}</p>
                                            <div class="price-box">
                            <span class="price">{$product.price}</span>
                            
                        </div>
                                    </div>
			
              {/foreach}    
       {/if}
        
    {elseif $poster.id_type_posters == 5}
<div id="viewed-products_block_left" class="block products_block">
 <h4 class="title_block">{$poster.posters_name}</h4>
        <!-- best sellers -->
{*<div id="best-sellers_block_right" class="block products_block">
    <h4 class="title_block">
        <a href="{$link->getPageLink('best-sales')|escape:'html'}" title="{l s='View a top sellers products' mod='blockbestsellers'}">
            {l s='Top sellers' mod='blockbestsellers'}
        </a>
    </h4>*}

    <div class="block_content">
        {if $best_sellers && $best_sellers|@count > 0}
            <ul class="product_images">
                {foreach from=$best_sellers item=product name=myLoop}
                    <li class="{if $smarty.foreach.myLoop.first}first_item{elseif $smarty.foreach.myLoop.last}last_item{else}item{/if} clearfix">
                        <a href="{$product.link|escape:'html'}" title="{$product.legend|escape:'html':'UTF-8'}" class="content_img clearfix">
                            <span class="number">{$smarty.foreach.myLoop.iteration}</span>
                            <img src="{$link->getImageLink($product.link_rewrite, $product.id_image, 'home_default')|escape:'html'}"
{*                                 height="{$mediumSize.height}" width="{$mediumSize.width}"*}
                                 alt="{$product.legend|escape:'html':'UTF-8'}"/>

                        </a>
                        {if !$PS_CATALOG_MODE}
                        <p>
                            <a href="{$product.link|escape:'html'}" title="{$product.legend|escape:'html':'UTF-8'}">
                                {$product.name|strip_tags:'UTF-8'|escape:'html':'UTF-8'}<br/>
                                {if !$PS_CATALOG_MODE}
                                    <span class="price">{$product.price}</span>
                                    {hook h="displayProductPriceBlock" product=$product type="price"}
                                {/if}
                            </a>
                        </p>
                        {/if}
                    </li>
                {/foreach}
            </ul>
          {*  <p class="lnk"><a href="{$link->getPageLink('best-sales')|escape:'html'}"
                              title="{l s='All best sellers' mod='blockbestsellers'}"
                              class="button_large">&raquo; {l s='All best sellers' mod='blockbestsellers'}</a></p>*}
        {else}
            <p>{l s='No best sellers at this time' mod='blockbestsellers'}</p>
        {/if}
    </div>
</div>
    {elseif $poster.id_type_posters == 6}
       <!-- Block Viewed products -->
<div id="viewed-products_block_left" class="block products_block">
	<h4 class="title_block">{$poster.posters_name}</h4>
	<div class="block_content">
		<ul class="products clearfix">
			{foreach from=$productsViewedObj item=viewedProduct name=myLoop}
				<li class="clearfix{if $smarty.foreach.myLoop.last} last_item{elseif $smarty.foreach.myLoop.first} first_item{else} item{/if}">
					<a href="{$viewedProduct->product_link|escape:'html'}" title="{l s='About' mod='blockviewed'} {$viewedProduct->name|escape:html:'UTF-8'}" class="content_img">
					<img src="{if isset($viewedProduct->id_image) && $viewedProduct->id_image}{$link->getImageLink($viewedProduct->link_rewrite, $viewedProduct->cover, 'medium_default')}{else}{$img_prod_dir}{$lang_iso}-default-medium_default.jpg{/if}" alt="{$viewedProduct->legend|escape:html:'UTF-8'}" title="{$viewedProduct->legend|escape:html:'UTF-8'}"/>
					</a>
					<div class="text_desc">
						<h5 class="s_title_block"><a href="{$viewedProduct->product_link|escape:'html'}" title="{l s='About' mod='blockviewed'} {$viewedProduct->name|escape:html:'UTF-8'}">{$viewedProduct->name|truncate:14:'...'|escape:html:'UTF-8'}</a></h5>
						<p><a href="{$viewedProduct->product_link|escape:'html'}" title="{l s='About' mod='blockviewed'} {$viewedProduct->name|escape:html:'UTF-8'}">{$viewedProduct->description_short|strip_tags:'UTF-8'|truncate:44}</a></p>
					</div>
				</li>
			{/foreach}
		</ul>
	</div>
</div>                
        
    {/if}
    {/foreach}

{/if}

