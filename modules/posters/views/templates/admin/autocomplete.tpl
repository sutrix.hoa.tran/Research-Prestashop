        <input type="hidden" name="id_product" id="inputMyAssociations" value="{foreach from=$my_associations item=accessory}{$accessory.id_product},{/foreach}" />
        <input type="hidden" name="nameMyAssociations" id="nameMyAssociations" value="{foreach from=$my_associations item=accessory}{$accessory.name|escape:'html':'UTF-8'}Â¤{/foreach}" />
        <div id="ajax_choose_product_association">
            <div class="input-group">
                <input type="text" id="product_autocomplete_input_association" name="product_autocomplete_input_association" />
                <span class="input-group-addon"><i class="icon-search"></i></span>
            </div>
        </div>

        <div id="divMyAssociations">
            {foreach from=$my_associations item=accessory}
                <div class="form-control-static">
                    <button type="button" class="btn btn-default delAccessory" name="{$accessory.id_product}">
                        <i class="icon-remove text-danger"></i>
                    </button>
                    {$accessory.name|escape:'html':'UTF-8'}{if !empty($accessory.reference)}{$accessory.reference}{/if}
                </div>
            {/foreach}
        </div>
    
