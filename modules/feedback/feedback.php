<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
if (!defined('_PS_VERSION_'))
   exit;
//require_once(dirname(__FILE__) . '/classes/Feedback.php');
class Feedback extends Module
{
    public function __construct() {
        
        $this->name = 'feedback';
        $this->tab = 'front_office_features';
        $this->version = '0.1';
        $this->author = 'Hoa Tran';
        $this->displayName = "Feedback";
        $this->description = 'With this module, you can get feedback from customers';
        parent::__construct();
        
    }
    
    public function install()
    {
        parent::install();
//        $this->registerHook('displayFooter');
        if(!$this->registerHook('displayRightColumn') 
            ||!$this->createTableFeedBack()){
            return false;
        }
        
        
        if (!$this->installTab('AdminCatalog', 'AdminFeedback','Feedback'))
               return false;
        
        return true;
    }
    
    public function hookDisplayRightColumn($params)
    {
//        $controller = $this->getHookController('displayRightColumn');
//        
//        return $controller;
          return $this->display(__FILE__, 'feedback.tpl');
    }


    public function uninstall()
    {
            if (!$this->uninstallTab('AdminFeedback'))
            return false;
            
        if(!parent::uninstall() || (!Db::getInstance()->execute('DROP TABLE  ps_feedback'))
                || (!Configuration::deleteByName('feedback'))){
            return false;
        }
   
    

        return true;
    }
    
    public function uninstallTab($class_name)
    {
        $id_tab = (int)Tab::getIdFromClassName($class_name);
        $tab = new Tab((int)$id_tab);

        return $tab->delete();
    }
    
    public function createTableFeedBack()
    {
        if(!Db::getInstance()->execute('CREATE TABLE IF NOT EXISTS `ps_feedback` (
	`id_feedback` INT(11) NOT NULL AUTO_INCREMENT,
	`subject` VARCHAR(50) NULL DEFAULT NULL,
	`email` VARCHAR(50) NULL DEFAULT NULL,
	`fileUpload` VARCHAR(50) NOT NULL,
	`message` VARCHAR(255) NULL DEFAULT NULL,
	PRIMARY KEY (`id_feedback`))ENGINE=InnoDB;'))
        {
            return false;
        }
        return true;
    }
    
    public function installTab($parent, $class_name, $name)
    {
        $tab = new Tab();
        $tab->id_parent = (int)Tab::getIdFromClassName($parent);
        $tab->name = array();
        foreach (Language::getLanguages(true) as $lang)
            $tab->name[$lang['id_lang']] = $name;
        $tab->class_name = $class_name;
        $tab->module = $this->name;
        $tab->active = 1;
        return $tab->add();
    }
    
    public function getHookController($hook_name)
    {
// Include the controller file
        require_once(dirname(__FILE__) . '/controllers/hook/' . $hook_name . '.php');
// Build the controller name dynamically
        $controller_name = $this->name . $hook_name . 'Controller';
// Instantiate controller
        $controller = new $controller_name($this, __FILE__, $this->_path);
// Return the controller
        return $controller;
    }
    
    public function processConfiguration() 
    {
        
    }
    
    public function assignConfiguration()
    {
        
    }

    public function getContent()
    {
        
        $this->processConfiguration();
        $html_confirmation_message = $this->display(__FILE__,'getContent.tpl');
        $html_form = $this->renderForm();
        return $html_confirmation_message.$html_form;
        
//        return $this->display(__FILE__,'getContent.tpl');
    }
    
    public function renderForm()
    {
         $fields_form = array(
            'legend' => array(
                'title' => $this->l('Feedback module'),
                'icon' => 'icon-truck'
            ),
            'input' => array(
                array(
                    'type' => 'text',
                    'label' => $this->l('Subject'),
                    'name' => 'subject',
                    'required' => true,
                    'col' => 4
                ),

                array(
                    'type' => 'text',
                    'label' => $this->l('Email'),
                    'name' => 'email',
                    'required' => true,
                    'maxlength' => 16,
                    'col' => 4,
                    'hint' => $this->l('Email of customer')
                ),
                array(
                    'type' => 'file',
                    'label' => $this->l('Attached file'),
                    'name' => 'fileUpload',
                    'required' => false,
                    'maxlength' => 50,
                    'col' => 4,
                    'hint' => $this->l('Attach file')
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Message'),
                    'name' => 'message',
                    'required' => true,
                    'maxlength' => 100,
                    'col' => 6,
                    'hint' => $this->l('Message of customer')
                ),

            ),
            'submit' => array(
                'title' => $this->l('Save'),
            )
        );
         $this->context =  Context::getContext();
         $helper = new HelperForm();
         $helper->table = 'feedback';
         $helper->default_form_language = (int)Configuration::get('PS_LANG_DEFAULT');
         $helper->allow_employee_form_lang = (int)Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG');
         $helper->submit_action = 'feedbackBtn';
         $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
         $helper->token = Tools::getAdminTokenLite('AdminModules');
         $helper->tpl_vars = array(
                'fields_value' => array(),
                'languages' => $this->context->controller->getLanguages());
        return $helper->generateForm(array($fields_form));

    }
    
    
    
}