<div class="container">
    <div class="panel-heading">Message of customner</div>
    <div class="row">
        <label class="control-label col-lg-3">{l s='Subject'}</label>
        <div class="col-lg-9">
            <p class="form-control-static">{$feedback->subject}</p>
        </div>
    </div>
        <div class="row">
            <label class="control-label col-lg-3">{l s='Email'}</label>
            <div class="col-lg-9">
                <p class="form-control-static">{if isset($feeback->email)}{$feeback->email}{/if}</p>
            </div>
        </div>
    <div class="row">
        <label class="control-label col-lg-3">{l s='Attached File'}</label>
        <div class="col-lg-9">
            <p class="form-control-static">{$feedback->fileUpload}</p>
        </div>
    </div>
    <div class="row">
        <labe class='control-label col-lg-3'>{l s='Message'}</labe>
        <div class='col-lg-9'>
            <p class='form-control-static'>{$feedback->message}</p>
        </div>
    </div>
</div>