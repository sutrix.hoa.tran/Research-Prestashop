{if isset($confirm)}
<p class="alert alert-success">{l s='Your message has been successfully sent to our team.'}</p>
    <ul class="footer_links clearfix">
	<li>
            <a class="btn btn-default button button-small" href="{if isset($force_ssl) && $force_ssl}{$base_dir_ssl}{else}{$base_dir}{/if}">
		<span>
		<i class="icon-chevron-left"></i>{l s='Home'}
		</span>
            </a>
	</li>
    </ul>
                
{else}
<h1 class="page-heading bottom-indent text-center">Send feedback to Sutrix Shop</h1>
     {include file="$tpl_dir./errors.tpl"}
<form enctype="multipart/form-data" method="POST" action="{$link->getModuleLink('feedback','management', [], true)|escape:'html'}" class="form-horizontal">
   
  <div class="form-group">
    <label class="control-label col-sm-2" for="email">Subject:</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" value="{if isset($subject)}{$subject}{/if}" name="subject" id="subject" placeholder="Enter Subject">
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-sm-2" for="pwd">Email:</label>
    <div class="col-sm-10"> 
        <input type="email" class="form-control" name="email" value="{if isset($email)} {$email} {/if}" id="pwd" placeholder="Enter Ema">
    </div>
  </div>
<div class="form-group">
    <label class="control-label col-sm-2" for="fileUpload">{l s='Select file'} <sup>*</sup></label>
    <div class="col-sm-10">
    <input type="hidden" name="MAX_FILE_SIZE" value="2000000" />
    <input type="file" class="required form-control" id="fileUpload" name="fileUpload" />
    </div>
</div>
  <div class="form-group">
    <label class="control-label col-sm-2" for="pwd">Message:</label>
    <div class="col-sm-10"> 
         <textarea class="form-control" rows="5" name="message" value = "{$message}" id="comment"></textarea>
    </div>
  </div>

  <div class="form-group"> 
    <div class="col-sm-offset-2 col-sm-10">
     <button type="submit" name='btnFeedback' id="submit" class="button btn btn-default button-medium"><span>{l s='Send'}<i class="icon-chevron-right right"></i></span></button>
    </div>
  </div>
</form>
{/if} 
    