{if isset($confirmation)}
    {if ($confirmation == 'ok')}
        <div class="alert alert-success">Test API Connection Success</div>
    {else}
        <div class="alert alert-error">Test API Connection Error</div>
    {/if}
{/if}
<fieldset>
    <h2>My module configuration</h2>
    <div class="panel">
        <div class="panel-heading">
            <legend>
                <img src="../img/admin/cog.gif" alt="" width="16"/>Configuration
            </legend>
        </div>
        <form class="form-horizontal" action="" method="post">
            <div class="form-group">
                <label class="controll-label col-sm-2" for="MYMOD_CA_EMAIL">E-mail</label>
                <div class="col-sm-10">
                    <input type="text" value="" name="MYMOD_CA_EMAIL">
                </div>
            </div>

            <div class="form-group">
                <label class="controll-label col-sm-2" for="MYMOD_CA_TOKEN">Token</label>
                <div class="col-sm-10">
                    <input type="text" value="" name="MYMOD_CA_TOKEN">
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button name="btnConfigForm" type="submit" class="btn btn-success">Save</button>
                </div>
            </div>

        </form>
    </div>
</fieldset>