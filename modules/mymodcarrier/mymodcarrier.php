<?php

/**
 * Created by PhpStorm.
 * User: hoa.tran
 * Date: 22/08/2016
 * Time: 13:39
 */
class MyModCarrier extends CarrierModule
{
    public $id_carrier;
    public function __construct()
    {
        $this->name = 'mymodcarrier';
        $this->tab = 'shipping_logistics';
        $this->version = '0.1';
        $this->author = 'Xuan Hoa';
        $this->bootstrap = true;
        parent::__construct();
        $this->displayName = $this->l('MyMod Carrier');
        $this->description = $this->l('A simple carrier module');

    }

    public function getOrderShippingCost($params, $shipping_cost)
    {
//        return 23;
//        return false;
//        $products = $params['cart']->getProducts(true);

        $controller = $this->getHookController('getOrderShippingCost');
        return $controller->run($params, $shipping_cost);
    }

    public function getOrderShippingCostExternal($params)
    {
//        return false;
        return $this->getOrderShippingCost($params, 0);
    }


//    public function getContent()
//    {
//        $controller = $this->getHookController('getContent');
//        return $controller->run();
//    }
    public function getContent()
    {
        $this->processConfiguration();
        $this->assignConfiguration();
        return $this->display(__FILE__, 'getContent.tpl');
    }

    public function getHookController($hook_name)
    {
// Include the controller file
        require_once(dirname(__FILE__) . '/controllers/hook/' . $hook_name . '.php');
// Build the controller name dynamically
        $controller_name = $this->name . $hook_name . 'Controller';
// Instantiate controller
        $controller = new $controller_name($this, __FILE__, $this->_path);
// Return the controller
        return $controller;
    }

    public function testAPIConnection($mca_email, $mca_token)
    {
        $url = 'http://prestashop-research.local/api/';
//        $params = '?mca_email=' . $mca_email . '&mca_token=' . $mca_token .
//            '&method=testConnection';
//        $urlParam  = $url.$params;

        $result = json_decode(file_get_contents($url), true);
//        var_dump($url);
//        var_dump(file_get_contents($url.$params));
//        var_dump($result);
//        exit;
        if ($result == 'Success')
            return true;
        return false;
    }

    public function processConfiguration()
    {
        if (Tools::isSubmit('btnConfigForm')) {
            Configuration::updateValue('MYMOD_CA_EMAIL', Tools::getValue('MYMOD_CA_EMAIL'));
            Configuration::updateValue('MYMOD_CA_TOKEN', Tools::getValue('MYMOD_CA_TOKEN'));
//            if ($this->testAPIConnection(Tools::getValue('MYMOD_CA_EMAIL'), Tools::getValue('MYMOD_CA_TOKEN'))) {
            if(1)  {
            $this->context->smarty->assign('confirmation', 'ok');
            } else {
                $this->context->smarty->assign('confirmation', 'faile');
            }
        }
    }

    public function assignConfiguration()
    {
        $email = Configuration::get('MYMOD_CA_EMAIL');
        $token = Configuration::get('MYMOD_CA_TOKEN');

        $this->context->smarty->assign('MYMOD_CA_EMAIL', $email);
        $this->context->smarty->assign('MYMOD_CA_TOKEN', $token);
    }

    public function install()
    {
        if (!parent::install()) {
            return false;
        }
        if (!$this->installCarriers()) {
            return false;
        }
        return true;
    }

    public function installCarriers()
    {
        $id_lang_default = Language::getIsoById(Configuration::get('PS_LANG_DEFAULT'));
//        $carriers_list = array(
//            'MYMOD_CA_CLDE' => 'Classic delivery',
//            'MYMOD_CA_REPO' => 'Relay Point',
//        );
        $carrier_key = "MYMOD_CA_DESH";
        $carrier_name = "Designer Shipping";

//        foreach ($carriers_list as $carrier_key => $carrier_name) {

                $carrier = new Carrier();
                $carrier->name = $carrier_name;
                $carrier->id_tax_rules_group = 0;
                $carrier->active = 1;
                $carrier->deleted = 0;
                foreach (Language::getLanguages(true) as $language)
                    $carrier->delay[(int)$language['id_lang']] = 'Delay' . $carrier_name;
                $carrier->shipping_handling = false;
                $carrier->range_behavior = 0;
                $carrier->is_module = true;
                $carrier->shipping_external = true;
                $carrier->external_module_name = $this->name;
                $carrier->need_range = true;
                if (!$carrier->add())
                    return false;
                // Associate carrier to all groups
                $groups = Group::getGroups(true);
                foreach ($groups as $group)
                    Db::getInstance()->insert('carrier_group',
                        array('id_carrier' => (int)$carrier->id, 'id_group' =>
                            (int)$group['id_group']));
                // Create price range
                $rangePrice = new RangePrice();
                $rangePrice->id_carrier = $carrier->id;
                $rangePrice->delimiter1 = '0';
                $rangePrice->delimiter2 = '10000';
                $rangePrice->add();
                // Create weight range
                $rangeWeight = new RangeWeight();
                $rangeWeight->id_carrier = $carrier->id;
                $rangeWeight->delimiter1 = '0';
                $rangeWeight->delimiter2 = '10000';
                $rangeWeight->add();
                // Associate carrier to all zones
                $zones = Zone::getZones(true);
                foreach ($zones as $zone) {
                    Db::getInstance()->insert('carrier_zone',
                        array('id_carrier' => (int)$carrier->id, 'id_zone' => (int)$zone['id_zone']));
                    Db::getInstance()->insert('delivery', array('id_carrier' => (int)$carrier->id, 'id_range_price' =>
                        (int)$rangePrice->id, 'id_range_weight' => NULL, 'id_zone' => (int)$zone['id_zone'], 'price' => '0'));
                    Db::getInstance()->insert('delivery', array('id_carrier' => (int)$carrier->id, 'id_range_price' => NULL,
                        'id_range_weight' => (int)$rangeWeight->id, 'id_zone'
                        => (int)$zone['id_zone'], 'price' => '0'));

                }
                // Copy the carrier logo
                copy(dirname(__FILE__).'/views/img/'.$carrier_key.'.jpg',
                    _PS_SHIP_IMG_DIR_.'/'.(int)$carrier->id.'.jpg');
                // Save the Carrier ID in the Configuration table
                Configuration::updateValue($carrier_key, $carrier->id);


        return true;
    }


}