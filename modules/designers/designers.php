<?php
/**
 * Created by PhpStorm.
 * User: hoa.tran
 * Date: 08/08/2016
 * Time: 15:15
 */

require_once(dirname(__FILE__) . '/classes/Designer.php');

class Designers extends Module
{
    public function __construct()
    {
        $this->uninstall();
        $this->name = 'designers';
        $this->tab = 'front_office_features';
        $this->version = '0.1';
        $this->author = 'Hoa Tran';
        $this->displayName = "Designers";
        $this->description = 'With this module, you can add designers for your product';
        parent::__construct();
    }


    public function install()
    {
        parent::install();
        $tab = new Tab();
        $this->tabClassName = 'AdminDesigners';
        $this->tabParentName = 'AdminCatalog';
        $tab->class_name = $this->tabClassName;
        if (Shop::isFeatureActive())
            Shop::setContext(Shop::CONTEXT_ALL);


//        $this->registerHook('displayProductTabContent');
        if (!$this->installTab($this->tabParentName, $this->tabClassName,
            'Designers')
        )
            return false;

        if (!$this->registerHook('displayAdminProductsExtra') ||
            !$this->registerHook('displayBackOfficeHeader') ||
            !$this->registerHook('displayLeftColumn') ||
            !$this->registerHook('ModuleRoutes')
        ) {
            return false;
        }
        $this->img_folder = _PS_IMG_DIR_.'des';
        
        if(!is_dir($this->img_folder) && !file_exists($this->img_folder))
	{
            mkdir($this->img_folder);
            chown($this->img_folder,fileowner(_PS_IMG_DIR_));
            chgrp($this->img_folder,filegroup(_PS_IMG_DIR_));
            chmod($this->img_folder,fileperms(_PS_IMG_DIR_));
	}
                
        // create table
        // create ss_designer
        Db::getInstance()->execute('CREATE TABLE IF NOT EXISTS `ps_ss_designer` (
                                    `id_ss_designer` INT(7) NOT NULL AUTO_INCREMENT,
                                    `image` VARCHAR(50) NULL DEFAULT \'0\',
                                    `address` VARCHAR(100) NULL DEFAULT \'0\',
                                    `email` VARCHAR(100) NULL DEFAULT \'0\',
                                    `telephone` VARCHAR(50) NULL DEFAULT \'0\',
                                    `active` TINYINT(1) NOT NULL DEFAULT \'0\',
                                    `id_feature_value` INT(11) NULL DEFAULT NULL,
                                    `shipping_fee` INT(11) NULL DEFAULT NULL,
                                    PRIMARY KEY (`id_ss_designer`)
                                 )
                                COLLATE=\'utf8_general_ci\'
                                ENGINE=InnoDB
                                AUTO_INCREMENT=1;');
        // create ss_designer_lang
        Db::getInstance()->execute('CREATE TABLE IF NOT EXISTS `ps_ss_designer_lang` (
                                    `id_ss_designer` INT(11) NOT NULL,
                                    `id_lang` INT(11) NOT NULL,
                                    `name` VARCHAR(100) NULL DEFAULT NULL,
                                    `url_name` VARCHAR(50) NULL DEFAULT NULL,
                                    `description` VARCHAR(255) NULL DEFAULT NULL,
                                    `meta_keywords` VARCHAR(255) NULL DEFAULT NULL,
                                    `meta_description` VARCHAR(255) NULL DEFAULT NULL,
                                    PRIMARY KEY (`id_ss_designer`, `id_lang`)
                                    )
                                    COLLATE=\'utf8_general_ci\'
                                    ENGINE=InnoDB;');
        // create table designer_shop
        Db::getInstance()->execute('CREATE TABLE IF NOT EXISTS `ps_ss_designer_shop` (
                                    `id_ss_designer` INT(7) NOT NULL,
                                    `id_shop` INT(11) NOT NULL,
                                    PRIMARY KEY (`id_ss_designer`, `id_shop`))
                                        COLLATE=\'utf8_general_ci\'
                                        ENGINE=InnoDB;');
        //Create table designer_supplier
        Db::getInstance()->execute('CREATE TABLE IF NOT EXISTS `ps_ss_designer_supplier` (
                                    `id_ss_designer_supplier` INT(11) NOT NULL AUTO_INCREMENT,
                                    `id_ss_designer` INT(11) NULL DEFAULT NULL,
                                    `id_ss_product` INT(11) NULL DEFAULT NULL,
                                    PRIMARY KEY (`id_ss_designer_supplier`),
                                    UNIQUE INDEX `id_ss_designer` (`id_ss_designer`),
                                    UNIQUE INDEX `id_ss_product` (`id_ss_product`))
                                COLLATE=\'utf8_general_ci\'
                                ENGINE=InnoDB
                                ;');


        return true;
    }

    public function installTab($parent, $class_name, $name)
    {
        $tab = new Tab();
        $tab->id_parent = (int)Tab::getIdFromClassName($parent);
        $tab->name = array();
        foreach (Language::getLanguages(true) as $lang)
            $tab->name[$lang['id_lang']] = $name;
        $tab->class_name = $class_name;
        $tab->module = $this->name;
        $tab->active = 1;
        return $tab->add();
    }

    public function uninstallTab($class_name)
    {
        $id_tab = (int)Tab::getIdFromClassName($class_name);
        $tab = new Tab((int)$id_tab);

        return $tab->delete();
    }

    public function processConfiguration()
    {

    }

    public function assignConfiguration()
    {

    }

    public function getContent()
    {
        $this->processConfiguration();
        $this->assignConfiguration();
        return $this->display(__FILE__, 'getContent.tpl');
    }

    public function uninstall()
    {
        if (!parent::uninstall() ||
            !Configuration::deleteByName('designers')
        )
            return false;
        if (!$this->uninstallTab('AdminDesigners'))
            return false;

        return true;
    }
////
//    public function hookDisplayAdminProductsExtra($params)
//    {
////        echo 'hello world';
//        return $this->display(__FILE__, 'displayAdminProductsExtra.tpl');
//
//    }

    public function getHookController($hook_name)
    {
// Include the controller file
        require_once(dirname(__FILE__) . '/controllers/hook/' . $hook_name . '.php');
// Build the controller name dynamically
        $controller_name = $this->name . $hook_name . 'Controller';
// Instantiate controller
        $controller = new $controller_name();
// Return the controller
        return $controller;
    }

    public function hookDisplayAdminProductsExtra($params)
    {
        $controller = $this->getHookController('AdminProductsExtra');
        return $controller->run();
    }

    public function hookModuleRoutes()
    {
        return array(
            'module-designers-designer' => array(
                'controller' => 'designer',
//                'rule' => 'product-designer{/:module_action}{/:id_ss_designer}/page{/:page}',
//                'rule' => 'designer{/:id_ss_designer}/page{/:page}',
                'rule' => 'designer{/:url_name}',
                'keywords' => array(
                    'url_name' => array(
                        'regexp' => '[\w]+',
                        'param' => 'url_name'),
//                    'page' => array(
//                        'regexp' => '[\d]+',
//                        'param' => 'page')
//                    'module_action' => array(
//                        'regexp' => '[\w]+',
//                        'param' => 'module_action'),
                ),
                'params' => array(
                    'fc' => 'module',
                    'module' => 'designers',
                    'controller' => 'designer'
                )
            )
        );

//        return array(
//            'module-designers-designer' => array(
//                'controller' => 'designer',
//                'rule' => 'designer{/:url_name}',
//                'keywords' => array(
//                    'url_name' => array(
//                        'regexp' => '[\w]+',
//                        'param' => 'url_name'),
//                ),
//                'params' => array(
//                    'fc' => 'module',
//                    'module' => 'designers',
//                    'controller' => 'designer'
//                )
//            )
//        );
    }

}
/*
 if(!defined('_PS_VERSION_')) exit;
require_once(dirname(__FILE__).'/classes/Designer.php');
class Designers extends Module
{
    public function __construct()
    {
        $this->name = 'designers';
        $this->tab = 'administration';
        $this->version = '1.0.0';
        $this->author = 'Thao Nguyen';
        $this->need_instance = 0;
        $this->ps_version_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Designers');
        $this->desciption = $this->l('Manage Designers');

        $this->confirmUninstall = $this->l('Are you sure you want to uninstall?');
        $this->img_folder = _PS_IMG_DIR_.'desg/';
        if(!defined('_PS_DESIGNER_IMG_DIR_'))
            define('_PS_DESIGNER_IMG_DIR_', $this->img_folder);
    }



    public function install()
    {
        if (Shop::isFeatureActive())
            Shop::setContext(Shop::CONTEXT_ALL);


        if (!parent::install() ||
            !$this->registerHook('leftColumn') ||
            !$this->registerHook('header') ||
            !$this->registerHook('displayProductTabContent') ||
            !$this->registerHook('displayBackOfficeHeader') ||
            !$this->registerHook('displayAdminProductsExtra') ||
            !$this->registerHook('displayHome') ||
            !$this->registerHook('displayHomeTab') ||
            !$this->registerHook('displayHomeTabContent') ||
            !$this->registerHook('ModuleRoutes') ||
            !$this->installTab('AdminCatalog','AdminDesigners','Designer')
        )
            return false;

        return true;
    }

    public function uninstall()
    {
        if (!parent::uninstall() ||
            !$this->uninstallTab('AdminDesigners')
        )
            return false;
        $this->uninstallTab('AdminSsDesigner');
        return true;
    }




    public function installTab($parent, $class_name, $name)
    {
        $tab = new Tab();
        $tab->id_parent	= (int)Tab::getIdFromClassName($parent);
        $tab->name = array();
        foreach (Language::getLanguages(true) as $lang)
            $tab->name[$lang['id_lang']] = $name;
        $tab->class_name = $class_name;
        $tab->module = $this->name;
        $tab->active = 1;
        return $tab->add();

    }


    public function uninstallTab($class_name)
    {
        $id_tab	= (int)Tab::getIdFromClassName($class_name);
        $tab = new Tab($id_tab);
        return $tab->delete();
    }



}
*/