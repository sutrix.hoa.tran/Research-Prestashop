<fieldset>
    <div class="panel">
        <div class="panel-heading">
            <legend><i class="icon-info"></i>
                {l s='Designer of product' mod='viewdesigner'}</legend>
        </div>
        <div class="form-group clearfix">
            <label class="col-lg-3">{l s='ID:' mod='viewdesigner'}</label>
            <div class="col-lg-9">{$viewdesigner->id}</div>
        </div>
        <div class="form-group clearfix">
            <label class="col-lg-3">{l s='Name:' mod='viewdesigner'}</label>
            <div class="col-lg-9">{$viewdesigner->name}</div>
        </div>
        <div class="form-group clearfix">
            <label class="col-lg-3">{l s='Phone:' mod='viewdesigner'}</label>
            <div class="col-lg-9">{$viewdesigner->telephone}</div>
        </div>
        <div class="form-group clearfix">
            <label class="col-lg-3">{l s='E-mail:' mod='viewdesigner'}</label>
            <div class="col-lg-9">{$viewdesigner->email}</div>
        </div>
        <div class="form-group clearfix">
            <label class="col-lg-3">{l s='Address:' mod='viewdesigner'}</label>
            <div class="col-lg-9">{$viewdesigner->address}</div>
        </div>
        <div class="form-group clearfix">
            <label class="col-lg-3">{l s='Description:' mod='viewdesigner'}</label>
            <div class="col-lg-9">{$viewdesigner->Description}</div>
        </div>
        <div class="form-group clearfix">
            <label class="col-lg-3">{l s='Enable:' mod='viewdesigner'}</label>
            <div class="col-lg-9">{$viewdesigner->is_active}</div>
        </div>
    </div>
</fieldset>