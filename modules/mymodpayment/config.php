<?php
/**
 * Created by PhpStorm.
 * User: hoa.tran
 * Date: 18/08/2016
 * Time: 09:13
 */
require_once(dirname(__FILE__) .'/views/js/stripe/init.php');
require_once(dirname(__FILE__) .'/views/js/stripe/lib/Stripe.php');


$modeStripe = Configuration::get('STRIPE_MODE');


if($modeStripe == 1){
    $secretKey = Configuration::get('STRIPE_TEST_SECRET');
    $publicKey = Configuration::get('STRIPE_TEST_PUBLIC');
}else{
    $secretKey = Configuration::get('STRIPE_LIVE_SECRETE');
    $publicKey = Configuration::get('STRIPE_LIVE_PUBLIC');
}

$stripe = array(
//    "secret_key"      => "sk_test_ugTYCLZVeEbwls1ZX8kP8qp2",
    "secret_key"      => $secretKey,
//    "publishable_key" => "pk_test_sULw7uQNl4mLArYnGUDqpyKs"
    "publishable_key" => $publicKey
);

\Stripe\Stripe::setApiKey($stripe['secret_key']);
?>
