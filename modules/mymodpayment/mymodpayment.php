<?php
/**
 * Created by PhpStorm.
 * User: hoa.tran
 * Date: 17/08/2016
 * Time: 13:38
 */
//class StripePayment extends PaymentModule
class MyModPayment extends PaymentModule
{
    public function __construct()
    {
        $this->name = 'mymodpayment';
        $this->tab = 'payments_gateways';
        $this->version = '0.1';
        $this->author = 'Xuan Hoa';
        $this->bootstrap = true;
        parent::__construct();
        $this->displayName = $this->l('My Stripe payment');
        $this->description = $this->l('A simple stripe payment module');
    }

    public function install()
    {
        if (Shop::isFeatureActive())
            Shop::setContext(Shop::CONTEXT_ALL);

        if (!parent::install() ||
            !$this->registerHook('displayPayment') ||
            !$this->registerHook('displayPaymentReturn') ||
            !$this->registerHook('displayOrderDetail') ||
            !$this->registerHook('displayPDFInvoice')
            
        )
            return false;

        // Install admin tab
        if (!$this->installTab('AdminOrders', 'AdminStripePayments',
            'Stripe Payments')
        )
            return false;


        return true;
    }

    public function uninstall()
    {
        if (!parent::uninstall() ||
            !Configuration::deleteByName('mymodpayment')
        )
            return false;
        if (!$this->uninstallTab('AdminStripePayments'))
            return false;

        return true;
    }

    public function installTab($parent, $class_name, $name)
    {
        $tab = new Tab();
        $tab->id_parent = (int)Tab::getIdFromClassName($parent);
        $tab->name = array();
        foreach (Language::getLanguages(true) as $lang)
            $tab->name[$lang['id_lang']] = $name;
        $tab->class_name = $class_name;
        $tab->module = $this->name;
        $tab->active = 1;
        return $tab->add();
    }

//    public function installTab($parrent, $className, $name)
//    {
//        $tab = new Tab();
//        $tab->id_parent = (int)Tab::getIdFromClassName($parrent);
//        $tab = array();
//        foreach (Language::getLanguages(true) as $lang)
//        {
//            $tab->name[$lang['id_lang']] = $name;
//        }
//        $tab->class_name =  $className;
//        $tab->module = $this->module;
//        $tab->active = 1;
//
//        return $tab->add();
//
//    }

    public function uninstallTab($class_name)
    {
        $id_tab = (int)Tab::getIdFromClassName($class_name);
        $tab = new Tab((int)$id_tab);

        return $tab->delete();
    }


    public function hookDisplayPayment($params)
    {
        $controller = $this->getHookController('displayPayment');
        return $controller->run($params);

//        return $this->display(__FILE__, 'displayPayment.tpl');
    }

    public function hookDisplayPaymentReturn($param)
    {
        $controller = $this->getHookController('paymentReturn');
//        return $this->display(__FILE__, 'paymentReturn.tpl');
        return $controller->run($param);
    }
    public function hookDisplayOrderDetail($param)
    {
//        return '<h2> I am here </h2>';
        $controller =  $this->getHookController('orderDetailInfo');
        return $controller->run($param);
        
    }
    
    public function hookDisplayPDFInvoice($param)
    {
         $controller =  $this->getHookController('displayPDFInvoice');
        return $controller->run($param);
    }

    public function getHookController($hook_name)
    {
// Include the controller file
        require_once(dirname(__FILE__) . '/controllers/hook/' . $hook_name . '.php');
// Build the controller name dynamically
        $controller_name = $this->name . $hook_name . 'Controller';
// Instantiate controller
        $controller = new $controller_name($this, __FILE__, $this->_path);
// Return the controller
        return $controller;
    }

//    public function getContent()
//    {
//        $controller = $this->getHookController('getContent');
//        return $controller->run();
//    }
//
    public function processConfiguration()
    {


        if (Tools::isSubmit('btnStripeConfigForm')) {

            $modeStripe = Tools::getValue('modeStripe');
            $testSecretKey = Tools::getValue('testSecretKey');
            $testPublicKey = Tools::getValue('testPublicKey');
            $liveSecretKey = Tools::getValue('liveSecretKey');
            $livePublicKey = Tools::getValue('livePublicKey');
            $namePayment = Tools::getValue('namePayment');
            $titlePayment = Tools::getValue('titlePayment');

//            var_dump($testSecretKey);
//            var_dump($testPublicKey);
//            var_dump($liveSecretKey);
//            var_dump($livePublicKey);
//            exit;
            Configuration::updateValue('STRIPE_MODE', $modeStripe);
            Configuration::updateValue('STRIPE_TEST_SECRET', $testSecretKey);
            Configuration::updateValue('STRIPE_TEST_PUBLIC', $testPublicKey);
            Configuration::updateValue('STRIPE_LIVE_SECRETE', $liveSecretKey);
            Configuration::updateValue('STRIPE_LIVE_PUBLIC', $livePublicKey);

            Configuration::updateValue('STRIPE_NAME_PAYMENT', $namePayment);
            Configuration::updateValue('STRIPE_TITLE_PAYMENT', $titlePayment);
            $this->context->smarty->assign('confirmation', 'ok');
        }


    }

    public function assignConfiguration()
    {
        $modeStripe = Configuration::get('STRIPE_MODE');
        $testSecretKey = Configuration::get('STRIPE_TEST_SECRET');
        $testPublicKey = Configuration::get('STRIPE_TEST_PUBLIC');
        $liveSecretKey = Configuration::get('STRIPE_LIVE_SECRETE');
        $livePublicKey = Configuration::get('STRIPE_LIVE_PUBLIC');

        $namePayment = Configuration::get('STRIPE_NAME_PAYMENT');
        $titlePayment = Configuration::get('STRIPE_TITLE_PAYMENT');

        $this->context->smarty->assign('modeStripe', $modeStripe);
        $this->context->smarty->assign('testSecretKey', $testSecretKey);
        $this->context->smarty->assign('testPublicKey', $testPublicKey);
        $this->context->smarty->assign('liveSecretKey', $liveSecretKey);
        $this->context->smarty->assign('livePublicKey', $livePublicKey);

        $this->context->smarty->assign('namePayment', $namePayment);
        $this->context->smarty->assign('titlePayment', $titlePayment);

    }

    public function getContent()
    {
        $this->processConfiguration();
        $this->assignConfiguration();
        return $this->display(__FILE__, 'getContent.tpl');
    }
    
    public function renderForm()
    {
        $this->fields_form = array(
            'form' => array(
                'legend'=>array(
                    'title' => $this->l('Feedback module configration')
                )
            ),
        );
    }

}