{*{literal}*}
{*<script type="text/javascript" src="https://js.stripe.com/v2/"></script>*}
{*<script type="text/javascript" src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>*}
{*{/literal}*}
{*<link rel="stylesheet" type="text/css" href="http://getbootstrap.com/dist/css/bootstrap.css">*}
{*<script type="text/javascript">*}
{*Stripe.setPublishableKey('{$stripePublicKey}');*}
{*</script>*}
{*{literal}*}
{*<script type="text/javascript">*}
{*$(document).ready(function() {*}

{*$('#payment-form').validate({*}
{*rules: {*}
{*email: {*}
{*minlength: 3,*}
{*maxlength: 15,*}
{*email: true,*}
{*required: true*}
{*},*}
{*cardNumber:{*}
{*minLength: 10,*}
{*maxlength: 19,*}
{*required: true,*}
{*creditcard: true*}
{*},*}
{*cvcNumber:{*}
{*required: true,*}
{*}*}
{*},*}
{*messages: {*}
{*},*}
{*highlight: function(element) {*}
{*$(element).closest('.form-group').addClass('has-error');*}
{*},*}
{*unhighlight: function(element) {*}
{*$(element).closest('.form-group').removeClass('has-error');*}
{*},*}
{*errorElement: 'span',*}
{*errorClass: 'help-block',*}
{*errorPlacement: function(error, element) {*}
{*if(element.parent('.input-group').length) {*}
{*error.insertAfter(element.parent());*}
{*} else {*}
{*error.insertAfter(element);*}
{*}*}
{*}*}
{*});*}

{*var $form = $('#payment-form');*}
{*$form.validate();*}
{*$form.on('submit', function(e){*}
{*var isvalidate = $form.valid();*}
{*if(isvalidate)*}
{*{*}
{*//                    e.preventDefault();*}
{*$form.find('#btnSubmit').prop('disabled', true);*}

{*// Request a token from Stripe:*}
{*Stripe.card.createToken($form, stripeResponseHandler);*}

{*// Prevent the form from being submitted:*}
{*return false;*}


{*}*}

{*});*}
{*});*}

{*function stripeResponseHandler(status, response) {*}
{*// Grab the form:*}

{*var $form = $('#payment-form');*}

{*if (response.error) { // Problem!*}
{*alert('Error');*}
{*console.log(response.error.message);*}
{*// Show the errors on the form:*}
{*$form.find('.payment-errors').text(response.error.message);*}
{*$form.find('.submit').prop('disabled', false); // Re-enable submission*}

{*} else { // Token was created!*}
{*alert('Token was created');*}
{*// Get the token ID:*}
{*var token = response.id;*}
{*console.log(token);*}
{*// Insert the token ID into the form so it gets submitted to the server:*}
{*$form.append($('<input type="hidden" name="stripeToken">').val(token));*}

{*// Submit the form:*}
{*$form.get(0).submit();*}
{*}*}
{*}*}
{*</script>*}

{*{/literal}*}

{*{capture name=path}*}
{*{l s='Stripe Payment' mod='mymodpayment'}*}
{*{/capture}*}

{*<h2>{l s='Order summary' mod='mymodpayment'}</h2>*}

{*{assign var='current_step' value='payment'}*}
{*{include file="$tpl_dir./order-steps.tpl"}*}

{*{if isset($nbProducts) && $nbProducts <= 0}*}
{*<p class="warning">{l s='Your shopping cart is empty.' mod='mymodpayment'}</p>*}
{*{else}*}

{*<h3>{l s='' mod='mymodpayment'}</h3>*}

{*<form action="{$link->getModuleLink('mymodpayment','validation', [], true)|escape:'html'}" method="POST" id="payment-form" class="form-horizontal">*}
{*<div class="row row-centered">*}
{*<div class="col-md-4 col-md-offset-4">*}
{*<div class="page-header text-center">*}
{*<h2 class="">{$titlePayment}</h2>*}
{*</div>*}
{*<div class="alert alert-danger" id="a_x200" style="display: none;"> <strong>Error!</strong> <span class="payment-errors"></span>*}
{*</div>*}
{*<span class="payment-success">*}

{*</span>*}
{*<fieldset>*}
{*<!-- Email -->*}
{*<div class="form-group">*}
{*<label class="col-sm-4 control-label" for="textinput">Email</label>*}
{*<div class="col-sm-6">*}
{*<input type="text" id = "email" name="email" maxlength="65" placeholder="Email" class="email form-control">*}
{*</div>*}
{*</div>*}
{*<!-- Card Number -->*}
{*<div class="form-group">*}
{*<label class="col-sm-4 control-label" for="textinput">Card Number</label>*}
{*<div class="col-sm-6">*}
{*<input type="text" data-stripe="number" id="cardnumber" name = "cardNumber" maxlength="19" placeholder="Card Number"*}
{*class="card-number form-control" required>*}
{*</div>*}
{*</div>*}

{*<!-- Expiry-->*}
{*<div class="form-group">*}
{*<label class="col-sm-4 control-label" for="textinput">Card Expiry Date</label>*}
{*<div class="col-sm-6">*}
{*<div class="col-sm-2">*}
{*<select  style="width: 100px" data-stripe="exp-month" class="card-expiry-month stripe-sensitive required form-control">*}
{*<script type="text/javascript">*}
{*var select = $(".card-expiry-month"),*}
{*month = new Date().getMonth() + 1;*}
{*for (var i = 1; i <= 12; i++) {*}
{*select.append($("<option value='"+i+"' "+(month === i ? "selected" : "")+">"+i+"</option>"))*}
{*}*}
{*</script>*}
{*</select>*}

{*</div>*}
{*<div class="col-sm-1">*}
{*<span class="no-padding" > / </span>*}
{*</div>*}
{*<div class="col-sm-3">*}

{*<select style="width: 100px" data-stripe="exp-year" class="card-expiry-year stripe-sensitive required form-control">*}
{*</select>*}
{*{literal} <script type="text/javascript">*}
{*var select = $(".card-expiry-year"),*}
{*year = new Date().getFullYear();*}

{*for (var i = 0; i < 12; i++) {*}
{*select.append($("<option value='"+(i + year)+"' "+(i === 0 ? "" : "")+">"+(i + year)+"</option>"))*}
{*}*}
{*</script>{/literal}*}
{*</div>*}




{*</div>*}
{*</div>*}

{*<!-- CVV -->*}
{*<div class="form-group">*}
{*<label class="col-sm-4 control-label" for="textinput">CVV/CVV2</label>*}
{*<div class="col-sm-3">*}
{*<input type="text" id="cvcNumber" name ="cvcNumber" data-stripe="cvc" placeholder="CVV" maxlength="4" class="card-cvc form-control">*}
{*</div>*}
{*</div>*}

{*<!-- Important notice -->*}
{*<div class="form-group  text-center">*}
{*<div class="panel panel-success">*}
{*<div class="panel-heading">*}
{*<h3 class="panel-title">Important notice</h3>*}
{*</div>*}
{*<div class="panel-body">*}
{*<p>Your card will be charged {$total_amount} after submit.</p>*}
{*</div>*}
{*</div>*}

{*<!-- Submit -->*}
{*<div class="control-group">*}
{*<div class="controls">*}
{*<center>*}
{*<button id = 'btnSubmit'class="btn btn-success" type="submit">Pay Now</button>*}
{*</center>*}
{*</div>*}
{*</div>*}
{*</fieldset>*}
{*</div>*}
{*</form>*}

{*<form action= "{$link->getModuleLink('mymodpayment','validation', [], true)|escape:'html'}" method="post">*}
{*<script src="https://checkout.stripe.com/checkout.js" class="stripe-button"*}
{*data-label="Pay With Stripe Payment"*}
{*data-key="{$stripePublicKey}"*}
{*data-description="You are buying on Sutrix shop"*}
{*data-amount="{$total_amount}"*}
{*data-locale="auto"*}
{*data-name="Stripe Payments"*}
{*data-image="{$path_stripe}views/img/logo.png">*}
{*//                    data-shipping-address="true"*}
{*//                    data-billing-address="true">*}

{*</script>*}

{*</form>*}

{*{/if}*}


<style type="text/css" media="screen">
    .has-error input {
        border-width: 2px;
    }

    .validation.text-danger:after {
        content: 'Validation failed';
    }

    .validation.text-success:after {
        content: 'Validation passed';
    }
</style>

{literal}
    <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.3/jquery.min.js"></script>
    <script src="http://stripe.github.io/jquery.payment/lib/jquery.payment.js"></script>
{/literal}
<script type="text/javascript">
    Stripe.setPublishableKey('{$stripePublicKey}');

</script>
{literal}
    <script type="text/javascript">

        jQuery(function ($) {
            $('[data-numeric]').payment('restrictNumeric');
            $('.cc-number').payment('formatCardNumber');
            $('.cc-exp').payment('formatCardExpiry');
            $('.cc-cvc').payment('formatCardCVC');
            $.fn.toggleInputError = function (erred) {
                this.parent('.form-group').toggleClass('has-error', erred);
                return this;
            };
            $('#payment-form').submit(function (e) {
                e.preventDefault();
                var cardType = $.payment.cardType($('.cc-number').val());
                $('.cc-number').toggleInputError(!$.payment.validateCardNumber($('.cc-number').val()));
                $('.cc-exp').toggleInputError(!$.payment.validateCardExpiry($('.cc-exp').payment('cardExpiryVal')));
                $('.cc-cvc').toggleInputError(!$.payment.validateCardCVC($('.cc-cvc').val(), cardType));
                $('.cc-brand').text(cardType);
                $('.validation').removeClass('text-danger text-success');
                $('.validation').addClass($('.has-error').length ? 'text-danger' : 'text-success');

                $('.cc-email').toggleInputError(!validateEmail($('.cc-email').val().trim()));
                var isValidCardNumber = $.payment.validateCardNumber($('.cc-number').val());
                var isValidCardExpiry = $.payment.validateCardExpiry($('.cc-exp').payment('cardExpiryVal'));
                var isValidCardCVC = $.payment.validateCardCVC($('.cc-cvc').val(), cardType);


                if (isValidCardNumber && isValidCardExpiry && isValidCardCVC) {
                    var expiry = $('.cc-exp').val();
                    var month = expiry.substr(0, 2);
                    var year = expiry.substr(expiry.length - 4);

                    $('#payment-form').find('.submit').prop('disabled', true);
                    Stripe.createToken({
                        name: $('.cc-email').val().trim(),
                        number: $('.cc-number').val(),
                        exp_month: month,
                        exp_year: year,
                        cvc: $('.cc-cvc').val()
                    }, stripeResponseHandler);
//                Stripe.card.createToken($form, stripeResponseHandler);
                    return false;
                }


            });
        });

        function validateEmail(email) {
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        }
        function stripeResponseHandler(status, response) {
            // Grab the form:

            var $form = $('#payment-form');

            if (response.error) { // Problem!
                alert('Error');
//                console.log(response.error.message);
                // Show the errors on the form:
                $form.find('.payment-errors').text(response.error.message);
                $form.find('.submit').prop('disabled', false); // Re-enable submission

            } else { // Token was created!
//                alert('Token was created');
                // Get the token ID:
                var token = response.id;
//                console.log(token);
                // Insert the token ID into the form so it gets submitted to the server:
                $form.append($('<input type="hidden" name="stripeToken">').val(token));

                // Submit the form:
                $form.get(0).submit();
            }
        }
    </script>
{/literal}

{capture name=path}
    {l s='Stripe Payment' mod='mymodpayment'}
{/capture}

<h2>{l s='Order summary' mod='mymodpayment'}</h2>

{assign var='current_step' value='payment'}
{include file="$tpl_dir./order-steps.tpl"}

{if isset($nbProducts) && $nbProducts <= 0}
    <p class="warning">{l s='Your shopping cart is empty.' mod='mymodpayment'}</p>
{else}
    <h3>{l s='' mod='mymodpayment'}</h3>
    <div class="page-header text-center">
        <h2 class="">{$titlePayment}</h2>
    </div>
    <div class="container">
        <form novalidate autocomplete="on" method="POST" id="payment-form"
              action="{$link->getModuleLink('mymodpayment','validation', [], true)|escape:'html'}">
            <div class="form-group">
                <label for="cc-email" class="control-label">Email</label>
                <input id="cc-email" type="e +mail" class="input-lg form-control cc-email" autocomplete="cc-email"
                       placeholder="Email" required>
            </div>

            <div class="form-group">
                <label for="cc-number" class="control-label">Card Number
                    <small class="text-muted">[<span class="cc-brand"></span>]</small>
                </label>
                <input id="cc-number" type="tel" class="input-lg form-control cc-number" autocomplete="cc-number"
                       placeholder="•••• •••• •••• ••••" required>
            </div>

            <div class="form-group">
                <label for="cc-exp" class="control-label">Card Expiry </label>
                <input id="cc-exp" type="tel" class="input-lg form-control cc-exp" autocomplete="cc-exp"
                       placeholder="•• / ••" required>
            </div>

            <div class="form-group">
                <label for="cc-cvc" class="control-label">Card CVC</label>
                <input id="cc-cvc" type="tel" class="input-lg form-control cc-cvc" autocomplete="off" placeholder="•••"
                       required>
            </div>

            {*<div class="form-group">*}
            {*<label for="numeric" class="control-label">Restrict numeric</label>*}
            {*<input id="numeric" type="tel" class="input-lg form-control" data-numeric>*}
            {*</div>*}

            {*<button type="submit" class="btn btn-lg btn-primary">Pay Now</button>*}
            <button type="submit" class="button btn btn-default button-medium" style="float: right;">
							<span>
								Pay Now
								<i class="icon-chevron-right right"></i>
							</span>
            </button>

            {*<h2 class="validation"></h2>*}
        </form>
    </div>
{/if}

