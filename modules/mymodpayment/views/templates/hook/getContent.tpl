{if isset($confirmation)}
    <div class="alert alert-success">Settings updated</div>
{/if}

<fieldset>
    <h2>My Module configuration</h2>
    <div class="panel">
        <div class="panel-heading">
            <legend><img src="../img/admin/cog.gif" alt="" width="16"
                />Configuration
            </legend>
        </div>
        <form class="form-horizontal" method="post" accion="">

            <div class="form-group">
                <label class="control-label col-sm-2" for="namePayment">Name of payment method:</label>
                <div class="col-sm-10">
                    <input type="text" value="{$namePayment}" class="form-control" id="pwd" name="namePayment"
                           placeholder="Name of payment method">
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-sm-2" for="livePubliccKey">Title of payment method:</label>
                <div class="col-sm-10">
                    <input type="text" value="{$titlePayment}" class="form-control" id="pwd" name="titlePayment"
                           placeholder="Title of payment method">
                </div>
            </div>


            <div class="form-group">

                <label class="control-label col-sm-2">
                    <span class="label-tooltip" data-toggle="tooltip" data-html="true" title=""
                      data-original-title="	Should be enabled when testing module.">Test
					</span>
                </label>

                <div class="col-sm-10"><span class="switch prestashop-switch fixed-width-lg">
                        <input type="radio" name="modeStripe" id="modeStripe_on" {if $modeStripe eq '1'}checked{/if} value="1">
										<label for="modeStripe_on">Yes</label>
                        <input type="radio" name="modeStripe" id="modeStripe_off"  {if empty($modeStripe) || $modeStripe eq
                            '0'}checked{/if}  value="0">
										<label for="modeStripe_off">No</label><a class="slide-button btn"></a>
                    </span>
                </div>

            </div>

            <div class="form-group">
                <label class="control-label col-sm-2" for="testSecretKey">Test Secret Key:</label>
                <div class="col-sm-10">
                    <input value="{$testSecretKey}" type="text" class="form-control" id="testSecretKey"
                           name="testSecretKey" placeholder="sk_test_ugTYCLZVeEbwls1ZX8kP8qp2">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="testPublicKey">Test Publishable Key:</label>
                <div class="col-sm-10">
                    <input type="text" value="{$testPublicKey}" class="form-control" id="pwd" name="testPublicKey"
                           placeholder="pk_test_sULw7uQNl4mLArYnGUDqpyKs">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="liveSecretKey">Live Secret Key:</label>
                <div class="col-sm-10">
                    <input type="text" value="{$liveSecretKey}" class="form-control" id="email" name="liveSecretKey"
                           placeholder="sk_live_xS20EHwxKUhc41YxB64L5qpO">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="livePubliccKey">Live Publishable Key:</label>
                <div class="col-sm-10">
                    <input type="text" value="{$livePublicKey}" class="form-control" id="pwd" name="livePublicKey"
                           placeholder="pk_live_KS3txgHijXUEs05dUIHf0VyE">
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button name="btnStripeConfigForm" type="submit" class="btn btn-success">Save</button>
                </div>
            </div>
        </form>
    </div>
</fieldset>