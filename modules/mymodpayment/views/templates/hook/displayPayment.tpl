

<div class="">
    <p class="payment_module">
        <a  class="stripe" href="{$link->getModuleLink('mymodpayment', 'payment', [], true)|escape:'html'}" title="{l s= $namePayment mod='mymodpayment'}">
            {*<img src="{$path_stripe}views/img/logo.png" alt="{l s='Pay by stripe module' mod='mymodpayment'}" width="100 " height="50" />*}
            {l s=$namePayment mod='mymodpayment'}
            {*<span class="glyphicon glyphicon-chevron-right"></span>*}
        </a>
    </p>

</div>

<style>
    p.payment_module a.stripe    {
        background: url({$path_stripe}views/img/logo.png) 0px 0px no-repeat #fbfbfb;
    }
    p.payment_module a.stripe:after, p.payment_module a.bankwire:after, p.payment_module a.cash:after{
        display: block;
        content: "\f054";
        position: absolute;
        right: 15px;
        margin-top: -11px;
        top: 50%;
        font-family: "FontAwesome";
        font-size: 25px;
        height: 22px;
        width: 14px;
        color: #777;
    }

</style>
{*<div class="col-xs-12">*}
    {*<p class="payment_module">*}
        {*<a class="cheque" href="http://prestashop-research.local/index.php?fc=module&amp;module=cheque&amp;controller=payment&amp;id_lang=1" title="Pay by check.">*}
            {*Pay by check <span>(order processing will be longer)</span>*}
        {*</a>*}
    {*</p>*}
{*</div>*}