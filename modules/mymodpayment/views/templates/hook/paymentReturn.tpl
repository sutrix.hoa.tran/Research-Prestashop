{if $status == 'ok'}
    <h3>{l s='Your order on %s  is complete successfully.' sprintf=$shop_name mod='mymodpayment'}</h3>
        <br /><br />
        {l s='Your check must include:' mod='cheque'}
        <br /><br />- {l s='Payment amount.' mod='cheque'} <span class="price"><strong>{$total_to_pay}</strong></span>
        {*<br /><br />- {l s='Payable to the order of' mod='cheque'} <strong>{if $chequeName}{$chequeName}{else}___________{/if}</strong>*}
        {*<br /><br />- {l s='Mail to' mod='cheque'} <strong>{if $chequeAddress}{$chequeAddress}{else}___________{/if}</strong>*}
        {if !isset($reference)}
            <br /><br />- {l s='Do not forget to insert your order number #%d.' sprintf=$id_order mod='mymodpayment'}
        {else}
            <br /><br />- {l s='Do not forget to insert your order reference %s.' sprintf=$reference mod='mymodpayment'}
        {/if}
        <br /><br />{l s='An email has been sent to you with this information.' mod='mymodpayment'}
        <br /><br /><strong>{l s='Your order will be sent as soon as we receive your payment.' mod='mymodpayment'}</strong>
        <br /><br />{l s='For any questions or for further information, please contact our' mod='mymodpayment'} <a href="{$link->getPageLink('contact', true)|escape:'html'}">{l s='customer service department.' mod='mymodpayment'}</a>.
    </p>
{else}
    <p class="warning">
        {l s='We have noticed that there is a problem with your order. If you think this is an error, you can contact our' mod='mymodpayment'}
        <a href="{$link->getPageLink('contact', true)|escape:'html'}">{l s='customer service department.' mod='mymodpayment'}</a>.
    </p>
{/if}