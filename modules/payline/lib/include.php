<?php
/*
http://www.payline.com

Payline module for Prestashop 1.5.x & 1.6.x - v1.6.2.2 - September 2016

Copyright (c) 2015 Monext
*/
if (!defined('_PS_BASE_URL_')){
	define('_PS_BASE_URL_', Tools::getShopDomain(true));
}


require_once('lib/paylineSDK.php');